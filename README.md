# Entorno virtual

1.  Primero instalar python y configurar proxy de ser necesario:
    C:\Users\<user>\AppData\Roaming\pip
    pip.ini:

        [global]
        proxy = http://proxyar.ternium.net:8080

En python 3.5:
virtualenv env --python=python3

En Python 3.6
python3 -m venv env

    .\.env\Scripts\activate.bat

linux
	
	source env/bin/activate

	
Commandos Git

	git config --global http.proxy http://proxyar.ternium.net:8080/
	
	git clone https://damiansima@bitbucket.org/damiansima/web_demo.git
	
Agregar archivos

	git add <files>
	
Ver estados	

	git status
	
Guardar cambios	

	
	git commit -m "msg"
	
	
Enviar cambios al servidor
	
	git push	
	
paquetes de dependencias

	pip freeze > requirements.txt

editar requirements.txt solo dejar lo minimo

instalar dependencias

    pip install -r requirements.txt


Compilar vue
	
	cd ./web_demo/app/client_vue/vue_app
	npm install
	npm run build
	
y para Probar en desarrollo

	npm run dev