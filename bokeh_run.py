from bokeh.embed import server_document
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Slider
from bokeh.plotting import figure
from bokeh.server.server import Server
from bokeh.themes import Theme
from tornado.ioloop import IOLoop

from bokeh.models import HoverTool
from bokeh.models import CustomJS

from bokeh.models import DatetimeTickFormatter, DatePicker
from bokeh.models.widgets import Button
from bokeh.layouts import layout, column, row
from datetime import datetime, timedelta
from math import radians


# from bokeh.sampledata.sea_surface_temperature import sea_surface_temperature

import numpy as np

from bokeh.models import ColumnDataSource, DataRange1d, Plot, LinearAxis, Grid
from bokeh.models.glyphs import Step
from bokeh.io import curdoc, show

import random
from datetime import datetime
import time
import math


def modify_doc(doc):
    df = sea_surface_temperature.copy()
    source = ColumnDataSource(data=df)

    plot = figure(x_axis_type='datetime', y_range=(0, 25), y_axis_label='Temperature (Celsius)',
                  title="Sea Surface Temperature at 43.18, -70.43")
    plot.line('time', 'temperature', source=source)

    def callback(attr, old, new):
        if new == 0:
            data = df
        else:
            data = df.rolling('{0}D'.format(new)).mean()
        source.data = ColumnDataSource(data=data).data

    slider = Slider(start=0, end=30, value=0, step=1,
                    title="Smoothing by N Days")
    slider.on_change('value', callback)

    doc.add_root(column(slider, plot))

    # doc.theme = Theme(filename="theme.yaml")


def modify_doc2(doc):

    N = 11
    x = np.linspace(-2, 2, N)
    y = x**2

    source = ColumnDataSource(dict(x=x, y1=y, y2=y+2, y3=y+4))

    xdr = DataRange1d()
    ydr = DataRange1d()

    # plot = Plot(
    #     title=None, x_range=xdr, y_range=ydr, plot_width=300, plot_height=300,
    #     h_symmetry=False, v_symmetry=False, min_border=0)

    plot = figure(x_axis_type='datetime', y_range=(0, 25), y_axis_label='Temperature (Celsius)',
                  title="Sea Surface Temperature at 43.18, -70.43")

    glyph1 = Step(x="x", y="y1", line_color="#f46d43", mode="before")
    plot.add_glyph(source, glyph1)

    glyph2 = Step(x="x", y="y2", line_dash="dashed",
                  line_color="#1d91d0", mode="center")
    plot.add_glyph(source, glyph2)

    glyph3 = Step(x="x", y="y3", line_width=3,
                  line_color="#cab2d6", mode="after")
    plot.add_glyph(source, glyph3)

    xaxis = LinearAxis()
    plot.add_layout(xaxis, 'below')

    yaxis = LinearAxis()
    plot.add_layout(yaxis, 'left')

    plot.add_layout(Grid(dimension=0, ticker=xaxis.ticker))
    plot.add_layout(Grid(dimension=1, ticker=yaxis.ticker))

    doc.add_root(plot)

    # doc.theme = Theme(filename="theme.yaml")


def modify_doc3(doc):
    source = ColumnDataSource({'x': [], 'y': [], 'color': []})

    def update():
        new = {'x': [random.random()],
               'y': [random.random()],
               'color': [random.choice(['red', 'blue', 'green'])]}
        print(new)
        source.stream(new)

    doc.add_periodic_callback(update, 1000)

    fig = figure(title='Streaming Circle Plot!',
                 x_range=[0, 1], y_range=[0, 1])
    fig.circle(source=source, x='x', y='y', color='color', size=10)

    doc.title = "Now with live updating!"
    doc.add_root(fig)


def modify_doc4(doc):
    source = ColumnDataSource({'x': [], 'y': [], 'y2': []})

    def update():
        t = time.time()
        new = {'x': [datetime.fromtimestamp(t)],
               'y': [math.sin(t/5)],
               'y2': [math.cos(t/8)]}

        source.stream(new, 1000)

    doc.add_periodic_callback(update, 100)

    hover = HoverTool(
        tooltips=[
            ("index", "$index"),
            ("(x,y)", "($x, $y)"),
            ("desc", "@desc"),
        ],
        mode='vline'
    )

    fig = figure(title='Streaming Circle Plot!',
                 x_axis_type='datetime')
    # fig.line('time', 'math.sin', source=source)
    s1 = fig.line(source=source, x='x', y='y')
    s2 = fig.line(source=source, x='x', y='y2', color='red')

    # hover = fig.select(dict(type=HoverTool))
    # hover = HoverTool(tooltips=[('Delay', '@f_interval'),
    #                            ('Num of Flights', '@f_flights')], mode='vline')
    # hover.tooltips = [("Series", "@series_name"),
    #                   ("Date", "@Date"),  ("Value", "@y{0.00%}"), ]

    fig.add_tools(hover)

    doc.title = "Now with live updating!"
    doc.add_root(fig)


def modify_doc5(doc):
    # Create figure
    f = figure(x_axis_type='datetime')

    # Create sample datetime data
    date_time = [datetime(2017, 1, 1) + timedelta(days=x)
                 for x in range(0, 365)]

    # Create ColumnDataSource
    source = ColumnDataSource(
        dict(datetime=date_time, parameter=range(0, 365)))

    # Create Line
    f.line(x='datetime', y='parameter', color='olive',
           line_color='black', source=source)

    # Update xaxis function
    def update_xaxis():
        # Calculate time delta from reference time in seconds
        timestamp_start = (datetime.combine(datepicker_start.value, datetime.min.time())
                           - datetime(1970, 1, 1)) / timedelta(seconds=1)
        timestamp_end = (datetime.combine(datepicker_end.value, datetime.min.time())
                         - datetime(1970, 1, 1)) / timedelta(seconds=1)
        # Multiply by 1e3 as JS timestamp is in milliseconds
        f.x_range.start = int(timestamp_start)*1e3
        # Multiply by 1e3 as JS timestamp is in milliseconds
        f.x_range.end = int(timestamp_end)*1e3

    # Set date format for x axis
    # f.xaxis.formatter = DatetimeTickFormatter(formats=dict(
    #     seconds=["%Y-%m-%d %H:%M:%S"],
    #     minsec=["%Y-%m-%d %H:%M:%S"],
    #     minutes=["%Y-%m-%d %H:%M:%S"],
    #     hourmin=["%Y-%m-%d %H:%M:%S"],
    #     hours=["%Y-%m-%d %H:%M:%S"],
    #     days=["%Y-%m-%d %H:%M:%S"],
    #     months=["%Y-%m-%d %H:%M:%S"],
    #     years=["%Y-%m-%d %H:%M:%S"],
    # ))

    # f.xaxis.major_label_orientation = radians(90)

    # Create Datepicker and Button widgets
    datepicker_start = DatePicker(title='Start Date')
    datepicker_end = DatePicker(title='End Date')
    button = Button(label='Set Date')

    # Update x axis range on click
    button.on_click(update_xaxis)

    # Add elements to curdoc
    lay_out = layout(
        [[row(f, column(button, row(datepicker_start, datepicker_end)))]])
    doc.add_root(lay_out)


# @app.route('/', methods=['GET'])
def bkapp_page():
    script = server_document('http://localhost:5006/bkapp')
    print("%r" % script)
    # return render_template("embed.1.html", script=script, template="Flask")


def bk_worker():
    # Can't pass num_procs > 1 in this configuration. If you need to run multiple
    # processes, see e.g. flask_gunicorn_embed.py
    server = Server({
        '/bkapp2': modify_doc2,
        "/bkapp3": modify_doc3,
        '/bkapp4': modify_doc4,
        '/bkapp5': modify_doc5
    },
        io_loop=IOLoop(), allow_websocket_origin=["*"])
    server.start()
    server.io_loop.start()


bk_worker()
