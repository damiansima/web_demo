""" Client App """

from flask import Blueprint, render_template

client_vue_bp = Blueprint(
    'client_vue',
    __name__,
    url_prefix='/client_vue',
    static_url_path='',
    static_folder='./vue_app/dist',
    template_folder='./vue_app/dist',
)


@client_vue_bp.route('/')
@client_vue_bp.route('/index')
def index():
    return render_template('index.html')
