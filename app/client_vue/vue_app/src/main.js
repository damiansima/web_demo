import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store";
import "./filters";
import VueSocketIO from "vue-socket.io";

const IS_PRODUCTION = process.env.NODE_ENV === "production";
Vue.config.productionTip = false;
const NAMESPACE = "/rt_data";

const WS_URL = IS_PRODUCTION
  ? location.protocol + "//" + document.domain + ":" + location.port + NAMESPACE
  : "http://localhost:5000" + NAMESPACE;

Vue.use(VueSocketIO, WS_URL, store);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
