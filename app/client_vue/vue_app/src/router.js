import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Api from "./views/Api.vue";
import Chat from "./views/Chat.vue";
import Plot from "./views/Plot.vue";
import Stock from "./views/Stock.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/api",
      name: "api",
      component: Api
    },
    {
      path: "/chat",
      name: "chat",
      component: Chat
    },
    {
      path: "/plot",
      name: "plot",
      component: Plot
    },
    {
      path: "/stock",
      name: "stock",
      component: Stock
    }
  ]
});
