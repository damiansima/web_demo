import axios from "axios";

const IS_PRODUCTION = process.env.NODE_ENV === "production";
const API_URL = IS_PRODUCTION ? "/api/" : "http://localhost:5000/api/";

let $axios = axios.create({
  baseURL: API_URL,
  timeout: 5000,
  headers: { "Content-Type": "application/json" }
});

// Request Interceptor
$axios.interceptors.request.use(function(config) {
  config.headers["Authorization"] = "Bearer " + localStorage.token;
  return config;
});

// Response Interceptor to handle and log errors
$axios.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    // Handle Error
    console.log(error);
    return Promise.reject(error);
  }
);

export default {
  fetchResource() {
    return $axios.get(`resource/xxx`).then(response => response.data);
  },

  fetchResourceId(resourseId) {
    return $axios
      .get(`echo_resource_id/` + resourseId)
      .then(response => response.data);
  },

  fetchSecureResource() {
    return $axios.get(`secure-resource/zzz`).then(response => response.data);
  },

  login(username, password) {
    return $axios
      .post("users/auth_token", {
        username: username,
        password: password
      })
      .then(response => {
        console.debug(response.data);
        localStorage.token = response.data.token;
        localStorage.username = response.data.username;
        localStorage.user_id = response.data.user_id;
      })
      .catch(() => {
        console.warn("login fail");
        this.logout();
      });
  },
  logout() {
    localStorage.token = null;
    localStorage.username = null;
    localStorage.user_id = null;
  },
  fetchToken() {
    return $axios
      .get(`users/token`)
      .then(response => {
        localStorage.token = response.data.token;
        localStorage.username = response.data.username;
        localStorage.user_id = response.data.user_id;
      })
      .catch(() => {
        // this.logout();
      });
  }
};
