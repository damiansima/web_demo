import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    isConnected: false,
    socketMessage: "",
    count: 1,
    todos: [
      { id: 1, text: "todo texto 1", done: true },
      { id: 2, text: "segundo texto todo 2", done: false }
    ],
    rtValue: { topic_demo: (1, 12.345) }
  },
  mutations: {
    SOCKET_CONNECT(state) {
      state.isConnected = true;
    },

    SOCKET_DISCONNECT(state) {
      state.isConnected = false;
    },

    SOCKET_RTVALUES(state, message) {
      message = message[0];
      // console.log("SOCKET_RTVALUES", message);
      state.rtValue = Object.assign({}, state.rtValue, message);
    },
    increment(state, n) {
      state.count += n;
    }
  },
  actions: {
    pingServer(context, obj) {
      console.log("action called pingServer");
      this.$socket.emit("pingServer", obj);
    }
  }
});
