from marshmallow_sqlalchemy import ModelSchema
from marshmallow import Schema, fields, validate, pre_load, post_dump, post_load
from app.model import users, generic_content
from app.model import session


class GroupSchema(ModelSchema):
    class Meta:
        model = users.Group
        sqla_session = session

    users = fields.Function(
        lambda obj: dict((u.user.id, u.user.username) for u in obj.users))


class UserGroupSchema(ModelSchema):
    class Meta:
        model = users.UserGroup
        sqla_session = session
    group = fields.Nested(GroupSchema, only=['name'])


class UserSchema(ModelSchema):
    class Meta:
        model = users.User
        sqla_session = session 
 
    groups = fields.Function(lambda obj: [u.group.name for u in obj.groups])

    uppername = fields.Function(lambda obj: obj.username.upper())
    token = fields.Function(
        lambda obj: obj.generate_auth_token().decode('ascii'))

    @post_load
    def make_user(self, data):
        return users.User(**data)


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


# class ModuleSchema2(ModelSchema):
#     class Meta:
#         model = permissions.Module
#         sqla_session = db.session
#     # permissions = fields.Function(lambda obj: dict(
#     #     (p.user.id, p.user.username) for p in obj.permissions))
# 
#     permissions = fields.Nested("ModulePermissionSchema", many=True)
#     groups = fields.Function(
#         lambda obj: [str(p.group.name)
#                      for p in obj.permissions if p.group is not None])
# 
#     users = fields.Method("get_user_groups", dump_only=True)
# 
#     def get_user_groups(self, obj):
#         users = dict()
#         for p in obj.permissions:
#             if p.owner is not None:
#                 users[p.owner.username] = AttrDict(
#                     c=p.c, r=p.r, u=p.u, d=p.d, is_owner=True)
# 
#         for p in obj.permissions:
#             if p.group is not None:
#                 for u in p.group.users:
#                     if u.user.username not in users:
#                         users[u.user.username] = AttrDict(
#                             c=p.c, r=p.r, u=p.u, d=p.d)
#                     else:
#                         crud = users[u.user.username]
#                         if not crud.c and p.c:
#                             crud.c = p.c
#                         if not crud.r and p.r:
#                             crud.r = p.r
#                         if not crud.u and p.u:
#                             crud.u = p.u
#                         if not crud.d and p.d:
#                             crud.d = p.d
#                         users[u.user.username] = crud
# 
#         return users
# 
# 
# class ModulePermissionSchema2(ModelSchema):
#     class Meta:
#         model = permissions.ModulePermission
#         sqla_session = db.session
# 
#     module = fields.Nested("ModuleSchema", only=['name', 'id'])
#     owner = fields.Nested("UserSchema", only=['username', 'id'])
#     group = fields.Nested("GroupSchema", only=['name', 'id'])
# 

# class GenericContentSchema(ModelSchema):
#     class Meta:
#         model = generic_content.GenericContent
#         sqla_session = db.session 
# 
#     # owner = fields.Nested("UserSchema", only=['username', 'id'])
#     # group = fields.Nested("GroupSchema", only=['name', 'id'])
#     # content = fields.Function(lambda obj: obj.content)
