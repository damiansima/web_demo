import sqlalchemy_pagination
from flask_restplus import reqparse
from app.utils import packer
from app.model import session


pagination_arguments = reqparse.RequestParser()
pagination_arguments.add_argument(
    'filters', type=str, required=False, help='Filter by ej: filters=[{"name": "id", "op": ">", "value": "8"}]')
pagination_arguments.add_argument(
    'order_by', type=str, required=False, help='Order by ej: order_by=[{"field": "id", "direction": "desc"}]')
pagination_arguments.add_argument(
    'page', type=int, required=False, default=1, help='Page number')
pagination_arguments.add_argument(
    'per_page', type=int, required=False, choices=[2, 10, 20, 30, 40, 50, 100],
    default=10, help='Results per page {error_msg}')


def paginate(schema_list, query):
    args = pagination_arguments.parse_args()
    page = args.get('page', 1)
    per_page = args.get('per_page', 10)

    pages = sqlalchemy_pagination.paginate(query, page, per_page)

    return dict(
        items=schema_list.dump(pages.items, many=True).data,
        pagination=dict(
            total_items=pages.total, has_next=pages.has_next, has_previous=pages.has_previous,
            page=page, per_page=per_page,
            total_pages=pages.pages, previous_page=pages.previous_page, next_page=pages.next_page
        )
    )


def get_filter_paginate(instance, schema_list):
    args = pagination_arguments.parse_args()

    fjs = args.get('filters')
    ojs = args.get('order_by')
    if fjs is not None:
        fjs = packer.UPK(fjs)
    if ojs is not None:
        ojs = packer.UPK(ojs)

    query = session.query(instance)
    print("****** get_filter_paginate ******")
    if ojs is not None or fjs is not None:
        f, o = to_filter(instance, fjs, ojs)
        query = query.filter(*f).order_by(*o)

    print("QUERY:", query)
    print("****** END ******")

    return paginate(schema_list, query)

    # f, o = to_filter(instance,,)
    # for i in f:
    #     print("--", i)
#
    # gg = users.User.query.filter(*f).order_by(*o)
    # print(gg)
    # print(gg.all())
    # print("end")

    # # Get all provided filters
    # argument_filters = self.get_query_argument_json("filters", [])
    # # Get all provided orders
    # argument_orders = self.get_query_argument_json("order_by", [])
    # return to_filter(model, argument_filters, argument_orders)


def to_filter(instance, filters=None, order_by=None):
    """
        Returns a list of filters made by arguments

        :param instance:
        :param filters: List of filters in restless 3-tuple op string format
        :param order_by: List of orders to be appended aswell
    """

    # Get all provided filters
    argument_filters = filters and filters or []

    # Parse order by as filters
    argument_orders = order_by and order_by or []

    for argument_order in argument_orders:
        direction = argument_order['direction']
        if direction not in ["asc", "desc"]:
            raise Exception("Direction unknown")
        argument_filters.append({'name': argument_order['field'], 'op': direction,
                                 'nullsfirst': argument_order.get('nullsfirst', False),
                                 'nullslast': argument_order.get('nullslast', False)})

    # Create Alchemy Orders
    alchemy_orders = []
    # Create Alchemy Filters
    alchemy_filters = []
    for argument_filter in argument_filters:

        print(" -", argument_filter)

        # Resolve right attribute
        if "field" in argument_filter.keys():
            right = getattr(instance, argument_filter["field"])
        elif "val" in argument_filter.keys():
            right = argument_filter["val"]
        elif "value" in argument_filter.keys():  # Because we hate abbr sometimes ...
            right = argument_filter["value"]
        else:
            right = None

        # Operator
        op = argument_filter["op"]

        # Resolve left attribute
        if "name" not in argument_filter:
            raise Exception("Missing fieldname attribute 'name'")

        if "__" in argument_filter["name"] or "." in argument_filter["name"]:
            relation, _, name = argument_filter["name"].replace(
                "__", ".").partition(".")
            left = getattr(instance, relation)
            op = "has"
            argument_filter["name"] = name
            argument_filter["op"] = "eq"
            right = to_filter(
                instance=left.property.mapper.class_, filters=[argument_filter])
        elif argument_filter["name"] == "~":
            left = instance
            op = "attr_is"
        else:
            left = getattr(instance, argument_filter["name"])

        print("   :", left, op, right)
        # Operators from flask-restless
        if op in ["is_null"]:
            alchemy_filters.append(left.is_(None))
        elif op in ["is_not_null"]:
            alchemy_filters.append(left.isnot(None))
        elif op in ["is"]:
            alchemy_filters.append(left.is_(right))
        elif op in ["is_not"]:
            alchemy_filters.append(left.isnot(right))
        elif op in ["==", "eq", "equals", "equals_to"]:
            alchemy_filters.append(left == right)
        elif op in ["!=", "ne", "neq", "not_equal_to", "does_not_equal"]:
            alchemy_filters.append(left != right)
        elif op in [">", "gt"]:
            alchemy_filters.append(left > right)
        elif op in ["<", "lt"]:
            alchemy_filters.append(left < right)
        elif op in [">=", "ge", "gte", "geq"]:
            alchemy_filters.append(left >= right)
        elif op in ["<=", "le", "lte", "leq"]:
            alchemy_filters.append(left <= right)
        elif op in ["ilike"]:
            alchemy_filters.append(left.ilike(right))
        elif op in ["not_ilike"]:
            alchemy_filters.append(left.notilike(right))
        elif op in ["like"]:
            alchemy_filters.append(left.like(right))
        elif op in ["not_like"]:
            alchemy_filters.append(left.notlike(right))
        elif op in ["match"]:
            alchemy_filters.append(left.match(right))
        elif op in ["in"]:
            alchemy_filters.append(left.in_(right))
        elif op in ["not_in"]:
            alchemy_filters.append(left.notin_(right))
        elif op in ["has"] and isinstance(right, list):
            alchemy_filters.append(left.any(*right))
        elif op in ["has"]:
            alchemy_filters.append(left.has(right))
        elif op in ["any"]:
            alchemy_filters.append(left.any(right))

        # Additional Operators
        elif op in ["between"]:
            alchemy_filters.append(left.between(*right))
        elif op in ["contains"]:
            alchemy_filters.append(left.contains(right))
        elif op in ["startswith"]:
            alchemy_filters.append(left.startswith(right))
        elif op in ["endswith"]:
            alchemy_filters.append(left.endswith(right))

        # Order By Operators
        elif op in ["asc", "desc"]:
            if argument_filter.get("nullsfirst", False):
                alchemy_orders.append(getattr(left, op)().nullsfirst())
            elif argument_filter.get("nullslast", False):
                alchemy_orders.append(getattr(left, op)().nullslast())
            else:
                alchemy_orders.append(getattr(left, op)())

        # Additional Checks
        elif op in ["attr_is"]:
            alchemy_filters.append(getattr(left, right))
        elif op in ["method_is"]:
            alchemy_filters.append(getattr(left, right)())

        # Test comparator
        elif hasattr(left.comparator, op):
            alchemy_filters.append(getattr(left.comparator, op)(right))

        # Raise Exception
        else:
            raise Exception("Unknown operator")
    return alchemy_filters, alchemy_orders
