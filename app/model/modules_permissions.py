from sqlalchemy import create_engine, Column, Integer, Unicode, BigInteger, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from app.model import session, Base, DeleteDb, UPdateDb, AddDb
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import Column, Integer, String, DateTime
from datetime import datetime
from sqlalchemy.orm import relationship, scoped_session, sessionmaker

from marshmallow_sqlalchemy import ModelSchema
from marshmallow import Schema, fields, validate, pre_load, post_dump, post_load

from functools import wraps
from flask import abort
from app.model.users import User, Group, UserGroup
from app.utils import memorize


class Module(Base):
    __tablename__ = 'module'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)

    permissions = relationship("ModulePermission", back_populates="module")

    def __repr__(self):
        return '<Module  %d "%r">' % (self.id or 0, self.name)


class ModulePermission(Base):
    __tablename__ = 'module_permission'
    module_id = Column(Integer, ForeignKey('module.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'),
                      primary_key=True, nullable=True)

    c = Column(Boolean)
    r = Column(Boolean)
    u = Column(Boolean)
    d = Column(Boolean)

    module = relationship("Module", back_populates="permissions")
    group = relationship("Group")

    def __repr__(self):
        return '<ModulePermission module_id:%d group_id:%d c:%s r:%s u:%s d:%s >' % (self.module_id or 0, self.group_id, self.c, self.r, self.u, self.d)


@memorize.memorize() 
def fn_permission_module(module_name: str, user: User, create=False, read=False, update=False, delete=False):

    if user.is_administrator:
        return True

    module = session.query(Module).filter_by(name=module_name).first()
    print("--- module name:", module_name)
    if not module:
        return False

    gps = session.query(ModulePermission).join(Group).join(UserGroup).filter(ModulePermission.module_id == module.id).filter(
        UserGroup.user_id == user.id).filter(UserGroup.group_id == ModulePermission.group_id).all()
    print("--- gps:", gps)
    for group_perm in gps:
        if group_perm:
            cheked = True
            if create and not group_perm.c:
                cheked = False
            if read and not group_perm.r:
                cheked = False
            if update and not group_perm.u:
                cheked = False
            if delete and not group_perm.d:
                cheked = False
            if cheked:
                return cheked
    return False


class ModuleSchema(ModelSchema):
    class Meta:
        model = Module
        sqla_session = session

    permissions = fields.Nested("ModulePermissionSchema", many=True)
    # groups = fields.Function(
    #     lambda obj: [str(p.group.name)
    #                  for p in obj.permissions if p.group is not None])

    users = fields.Method("get_user_groups", dump_only=True)


class ModulePermissionSchema(ModelSchema):
    class Meta:
        model = ModulePermission
        sqla_session = session

    module = fields.Nested("ModuleSchema", only=['name', 'id'])
    group = fields.Nested("GroupSchema", only=['name', 'id'])
