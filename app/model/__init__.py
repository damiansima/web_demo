
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# from flask_sqlalchemy import SQLAlchemy

engine = create_engine(u'sqlite:///db.sqlite')
# session = scoped_session(sessionmaker(bind=engine, autoflush=False))
Base = declarative_base()

Session = sessionmaker()
Session.configure(bind=engine, autoflush=True)
session = Session()

Base.query = property(lambda x:  session.query(x))

# db = SQLAlchemy()
# db.init_app(app)
# with app.test_request_context():
#     db.create_all()


def AddDb(obj):
    try:
        session.add(obj)
        session.commit()
        return True
    except:
        session.rollback()
        return False


def UPdateDb(obj):
    try:
        session.commit()
        return True
    except:
        session.rollback()
        return False


def DeleteDb(obj):
    try:
        session.delete(obj)
        session.commit()
        return True
    except:
        session.rollback()
        return False
