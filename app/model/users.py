#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from sqlalchemy import create_engine, Column, Integer, Unicode, BigInteger, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from app.model import session, Base, DeleteDb, UPdateDb, AddDb
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer,
                          BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context
from datetime import datetime
from app.utils import memorize

from app import app
SECRET_KEY = app.config['SECRET_KEY']


class Group(Base):
    __tablename__ = 'groups'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)

    users = relationship("UserGroup", back_populates="group")
    # modules = relationship("ModulePermission", back_populates="group")


class User(Base):
    __tablename__ = 'users'

    # session.query(User).filter(User.username == username).first()
    # user = User.query.filter_by(username=username).first()

    # id = Column(Integer, primary_key=True)
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(32), index=True, unique=True)
    password_hash = Column(String(64))
    created_at = Column(DateTime)
    name = Column(String, nullable=False)
    email = Column(String)

    groups = relationship("UserGroup", back_populates="user")

    # modules = relationship("ModulePermission", back_populates="user")

    def __repr__(self):
        return '<User %d %r "%r">' % (self.id or 0, self.username, self.name)

    def is_administrator(self, group_admin: str="Admin"):
        user_admin = session.query(User).join(UserGroup).join(
            Group).filter(Group.name == group_admin).filter(User.id == self.id).first()
        return user_admin is not None

    @staticmethod
    def add_user(username, name, password):
        if session.query(User).filter_by(username=username).first() is not None:
            return None
        user = User(username=username, name=name, created_at=datetime.utcnow())
        user.hash_password(password)
        if AddDb(user):
            group = session.query(Group).filter(
                Group.name == "Visualization").first()
            if group:
                ug = UserGroup(user=user, group=group)
                AddDb(ug)
            return user
        return None

    def hash_password(self, password):
        # fixme
        self.password_hash = pwd_context.encrypt(password)
        # self.password_hash = password

    def verify_password(self, password):
        # fixme
        return pwd_context.verify(password, self.password_hash)
        # return password == self.password_hash

    def generate_auth_token(self, expiration=600):
        print("generate_auth_token id", self.id)
        s = Serializer(SECRET_KEY, expires_in=expiration)
        return s.dumps({'id': self.id, 'username': self.username})

    @staticmethod
    @memorize.memorize()
    def get_user_by_name(username: str):
        return session.query(User).filter_by(username=username).first()
        # user = User.query.filter(User.username == username).first()
        # print("get_user_by_name User.query", user)
        # return user

    @staticmethod
    @memorize.memorize()
    def get_user_by_id(user_id: int):
        return session.query(User).filter(User.id == user_id).first()

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        user = User.get_user_by_id(data['id'])
        if user is None or user.username != data['username']:
            return None
        return user


class UserGroup(Base):
    __tablename__ = 'users_groups'
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)

    user = relationship("User", back_populates="groups")
    group = relationship("Group", back_populates="users")


def create_example_users():
    from pprint import pprint
    print("**** create_example_users start...")
    u1 = User.add_user(username="sima", name="Damian Simañuk", password="123")
    u2 = User.add_user(username="pepe", name="Pepe Simañuk", password="123")
    u3 = User.add_user(
        username="alfred", name="Alfredo Simañuk", password="123")
    pprint(u1)
    pprint(u2)
    pprint(u3)

    g1 = Group(name="Admin")
    g2 = Group(name="Editor")
    g3 = Group(name="Visualization")

    pprint(g1)
    pprint(g2)
    pprint(g3)
    AddDb(g1)
    AddDb(g2)
    AddDb(g3)

    ug1 = UserGroup(user=u1, group=g1)
    ug2 = UserGroup(user=u1, group=g2)
    ug3 = UserGroup(user=u2, group=g2)
    ug4 = UserGroup(user=u3, group=g3)
    pprint(ug1)
    pprint(ug2)
    pprint(ug3)
    pprint(ug4)
    AddDb(ug1)
    AddDb(ug2)
    AddDb(ug3)
    AddDb(ug4)

    for un in ['Damian', 'Simon', 'Guillermo', 'Gustavo', 'Otro']:
        ux = User.add_user(username=un, name="%s Simañuk" % un, password="123")
        ugx = UserGroup(user=ux, group=g3)
        AddDb(ugx)
