from sqlalchemy import create_engine, Column, Integer, Unicode, BigInteger, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from app.model import db, DeleteDb, UPdateDb, AddDb
from app.model import db
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import Column, Integer, String, DateTime
from datetime import datetime
from sqlalchemy.orm import relationship, scoped_session, sessionmaker

session = db.session


# class Permission(db.Model):
#     __tablename__ = 'permission'
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     name = Column(String, unique=True, nullable=False)
#     description = Column(String)

#     groups = relationship("PermissionGroup", back_populates="permission")

# class PermissionGroup(db.Model):
#     __tablename__ = 'permissions_groups'
#     permission_id = Column(Integer, ForeignKey(
#         'permission.id'), primary_key=True)
#     group_id = Column(Integer, ForeignKey('groups.id'), primary_key=True)

#     permission = relationship("Permission", back_populates="groups")
#     group = relationship("Group", back_populates="permissions")


class Module(db.Model):
    __tablename__ = 'module'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String)

    permissions = relationship(
        "ModulePermission", back_populates="module")
    # permissions = relationship("PermissionGroup", back_populates="group")


class ModulePermission(db.Model):
    __tablename__ = 'module_permission'
    module_id = Column(Integer, ForeignKey('module.id'), primary_key=True)
    owner_id = Column(Integer, ForeignKey('users.id'),
                      primary_key=True, nullable=True)
    group_id = Column(Integer, ForeignKey('groups.id'),
                      primary_key=True, nullable=True)

    c = Column(Boolean)
    r = Column(Boolean)
    u = Column(Boolean)
    d = Column(Boolean)

    module = relationship("Module", back_populates="permissions")
    owner = relationship("User")
    group = relationship("Group")


def create_example():
    from pprint import pprint
    print("**** create_example start...")
    u1 = User(username="sima", name="Damian Simañuk", password_hash="123")
    u2 = User(username="pepe", name="Pepe Simañuk", password_hash="123")
    u3 = User(username="alfred", name="Alfredo Simañuk", password_hash="123")
    pprint(u1)
    pprint(u2)
    pprint(u3)
    AddDb(u1)
    AddDb(u2)
    AddDb(u3)

    g1 = Group(name="Admin")
    g2 = Group(name="Editor")
    g3 = Group(name="Visualization")

    pprint(g1)
    pprint(g2)
    pprint(g3)
    AddDb(g1)
    AddDb(g2)
    AddDb(g3)

    ug1 = UserGroup(user=u1, group=g1)
    ug2 = UserGroup(user=u1, group=g2)
    ug3 = UserGroup(user=u2, group=g2)
    ug4 = UserGroup(user=u3, group=g3)
    pprint(ug1)
    pprint(ug2)
    pprint(ug3)
    pprint(ug4)
    AddDb(ug1)
    AddDb(ug2)
    AddDb(ug3)
    AddDb(ug4)

    for un in ['Damian', 'Simon', 'Guillermo', 'Gustavo', 'Otro']:
        ux = User(username=un, name="%s Simañuk" % un, password_hash="123")
        AddDb(ux)
        ugx = UserGroup(user=ux, group=g3)
        AddDb(ugx)

    m = Module(name="Modulo1")
    m2 = Module(name="Modulo2")
    pprint(m)
    pprint(m2)
    AddDb(m)
    AddDb(m2)
    mp1 = ModulePermission(module=m, owner=u2, group=g2,
                           c=True, r=True, u=False, d=False)
    mp2 = ModulePermission(module=m, owner=u1, group=g2,
                           c=True, r=True, u=True, d=True)

    mp3 = ModulePermission(module=m2, group=g3,
                           c=True, r=True, u=False, d=False)
    mp4 = ModulePermission(module=m2, owner=u1,
                           c=True, r=True, u=True, d=True)
    pprint(mp1)
    pprint(mp2)
    pprint(mp3)
    pprint(mp4)
    AddDb(mp1)
    AddDb(mp2)
    AddDb(mp3)
    AddDb(mp4)

    from app.schema import user_schema
    from pprint import pprint

    mod = Module.query.filter(Module.name == "Modulo2").first()
    pprint(user_schema.ModuleSchema(many=False).dump(mod).data)
