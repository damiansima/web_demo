from sqlalchemy import Column, Integer, Unicode, BigInteger, String, ForeignKey, DateTime, Boolean
from sqlalchemy.orm import relationship
from datetime import datetime
from app.model import session, Base, DeleteDb, UPdateDb, AddDb
from app.model.users import User, Group, UserGroup
from app.utils import packer

from marshmallow_sqlalchemy import ModelSchema
from marshmallow import Schema, fields, validate, pre_load, post_dump, post_load

from functools import wraps
from flask import abort


class GenericContent(Base):
    __tablename__ = 'generic_content'
    id = Column(Integer, primary_key=True, autoincrement=True)

    content_name = Column(String(128), unique=True, nullable=False)
    content_json = Column(String, nullable=False)

    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, default=datetime.utcnow())

    permissions = relationship("ContentPermission", back_populates="content")

    @property
    def content(self):
        return packer.UPK(self.content_json)

    @staticmethod
    def add_content(username: str, content_name: str, content: dict):
        content_json = packer.PK(content)
        cont = GenericContent(
            content_name=content_name,
            content_json=content_json)

        if AddDb(cont):

            user = session.query(User).filter(
                User.username == username).first()
            ContentPermission.set_user_content_permission(
                cont.id, user, create=True, read=True, update=True, delete=False)

            group = session.query(Group).filter(
                Group.name == "Visualization").first()
            ContentPermission.set_group_content_permission(
                cont.id, group, create=False, read=True, update=False, delete=False)

            print("add_content add user:%s group:%s" % (user, group))

            return cont
        return None


class ContentPermission(Base):
    __tablename__ = 'content_permission'
    content_id = Column(Integer, ForeignKey(
        'generic_content.id'), primary_key=True, nullable=False)
    owner_id = Column(Integer, ForeignKey('users.id'),
                      primary_key=True, nullable=True)
    group_id = Column(Integer, ForeignKey('groups.id'),
                      primary_key=True, nullable=True)

    create = Column(Boolean)
    read = Column(Boolean)
    update = Column(Boolean)
    delete = Column(Boolean)

    content = relationship("GenericContent", back_populates="permissions")
    owner = relationship("User")
    group = relationship("Group")

    def __repr__(self):
        return '<ContentPermission Content:%r User:%r Group:%r>' % (self.content_id, self.owner_id or None, self.group_id or None)

    @staticmethod
    def user_allowed(content_id: int, user_id: int, create=False, read=False, update=False, delete=False):
        user_perm = session.query(ContentPermission).filter_by(
            content_id=content_id, owner_id=user_id).first()

        if user_perm:
            cheked = True
            if create and not user_perm.create:
                cheked = False
            if read and not user_perm.read:
                cheked = False
            if update and not user_perm.update:
                cheked = False
            if delete and not user_perm.delete:
                cheked = False
            return cheked
        return False

    @staticmethod
    def set_user_content_permission(content_id: int, user: User, create=False, read=True, update=False, delete=False):
        user_perm = session.query(ContentPermission).filter_by(
            content_id=content_id, owner_id=user.id).first()
        print("set_user_content_permission: ", content_id, user, user_perm)
        if user:
            if not user_perm:
                up = ContentPermission(
                    content_id=content_id, owner=user, create=create, read=read, update=update, delete=delete)
                return AddDb(up)
            else:
                user_perm.create = create
                user_perm.read = read
                user_perm.update = update
                user_perm.delete = delete
                return UPdateDb(user_perm)

    @staticmethod
    def set_group_content_permission(content_id: int, group: Group, create=False, read=True, update=False, delete=False):
        group_perm = session.query(ContentPermission).filter_by(
            content_id=content_id, group_id=group.id).first()

        print("set_group_content_permission: ", content_id, group, group_perm)
        if group:
            if not group_perm:
                print("group_perm Insert")
                gp = ContentPermission(
                    content_id=content_id, owner_id=None, group_id=group.id, create=create, read=read, update=update, delete=delete)
                print(gp)
                return AddDb(gp)
            else:
                print("group_perm Update")
                group_perm.create = create
                group_perm.read = read
                group_perm.update = update
                group_perm.delete = delete
                return UPdateDb(group_perm)


class ContentPermissionSchema(ModelSchema):
    class Meta:
        model = ContentPermission
        sqla_session = session

    owner = fields.Nested("UserSchema", only=['username', 'id'])
    group = fields.Nested("GroupSchema", only=['name', 'id'])
    content = fields.Nested("GenericContentSchema",
                            only=['content_name', 'id'])


class GenericContentSchema(ModelSchema):
    class Meta:
        model = GenericContent
        sqla_session = session

    # permissions = fields.Nested("ContentPermissionSchema" )
    # permissions = fields.Nested("ContentPermissionSchema", many=True)
    content = fields.Function(lambda obj: obj.content)
    groups = fields.Function(
        lambda obj: [dict(name=p.group.name, id=p.group.id, create=p.create, read=p.read, update=p.update, delete=p.delete)
                     for p in obj.permissions if p.group is not None])
    owners = fields.Function(
        lambda obj: [dict(name=p.owner.username, id=p.owner.id, create=p.create, read=p.read, update=p.update, delete=p.delete)
                     for p in obj.permissions if p.owner is not None])


def fn_permission_content(content_id: int, user: User, create=False, read=False, update=False, delete=False):
    user_perm = session.query(ContentPermission).filter_by(
        content_id=content_id, owner_id=user.id).first()

    if user_perm:
        cheked = True
        if create and not user_perm.create:
            cheked = False
        if read and not user_perm.read:
            cheked = False
        if update and not user_perm.update:
            cheked = False
        if delete and not user_perm.delete:
            cheked = False
        return cheked

    for group_cont_perm in session.query(ContentPermission).join(Group).join(UserGroup).filter(ContentPermission.content_id == content_id).filter(UserGroup.user_id == user.id).all():
        if group_cont_perm:
            cheked = True
            if create and not group_cont_perm.create:
                cheked = False
            if read and not group_cont_perm.read:
                cheked = False
            if update and not group_cont_perm.update:
                cheked = False
            if delete and not group_cont_perm.delete:
                cheked = False
            if cheked:
                return cheked
    return False
