import os
from flask_socketio import SocketIO, emit
import datetime
from app.utils import packer
import paho.mqtt.client as mqtt

broker_address = "127.0.0.1"
broker_port = 1883


mqttc = mqtt.Client("P1")
broker_address = "127.0.0.1"
broker_port = 1883
mqttc.connect_async(broker_address, broker_port, 10)

# async_mode: ``eventlet`` is tried first, then ``gevent_uwsgi``, then ``gevent``, and finally ``threading``.
socketio = SocketIO(async_mode=None)


@socketio.on('mqtt_publish')
def handle_publish(json_str):
    data = packer.UPK(json_str)
    mqttc.publish(data['topic'], data['payload'],
                  data.get('qos', 0), data.get('retain', 0))


@socketio.on('mqtt_subscribe')
def handle_subscribe(json_str):
    data = packer.UPK(json_str)
    mqttc.subscribe(data['topic'], data.get('qos', 0))


def on_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode(),
        retain=message.retain or False,
        qos=message.qos or 0
    )
    socketio.emit('mqtt_on_message', data=data)


def on_disconnect(client, userdata, self):
    print("on_disconnect client", client)
    print("userdata", userdata)
    print("self", self)


def on_connect(client, userdata, flags, rc):
    print("on_connect client", client)
    print("userdata", userdata)
    print("flags", flags)
    print("rc", rc)


# mqttc.on_message = on_message
# mqttc.on_disconnect = on_disconnect
# mqttc.on_connect = on_connect
# mqttc.subscribe("$SYS/#", 0)


@socketio.on('pingServer', namespace='/test')
def test_pingServer(message):
    print("test_pingServer", str(message))
    emit('my_response', str(message) + " " + str(datetime.datetime.now()))


@socketio.on('my_event', namespace='/test')
def test_message(message):
    print({'data': message['data']})
    emit('my_response', {'data': message['data']})


@socketio.on('my broadcast event', namespace='/test')
def test_broadcast_message(message):
    emit('my response', {'data': message['data']}, broadcast=True)


@socketio.on('connect', namespace='/test')
def test_connect():
    emit('my response', {'data': 'Connected'})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')
