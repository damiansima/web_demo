import paho.mqtt.client as mqtt
import time

broker_address = "127.0.0.1"
broker_port = 1883


print("creating new instance")
client = mqtt.Client("P1")


print("connecting to broker")
client.connect(broker_address, broker_port)


def on_message(client, userdata, message):
    print("message received ", str(message.payload.decode("utf-8")))
    print("message topic=", message.topic)
    print("message qos=", message.qos)
    print("message retain flag=", message.retain)


def disconnect_callback(client, userdata, self):
    print("client", client)
    print("userdata", userdata)
    print("self", self)


client.on_message = on_message
client.on_disconnect = disconnect_callback


client.loop_start()
client.subscribe("house/bulbs/bulb1")
client.publish("house/bulbs/bulb1", "OFF")

time.sleep(10)

client.loop_stop()
time.sleep(10)
