#!/usr/bin/env python

from app.socketio import socketio, mqttc
from app.utils import packer

from threading import Lock, Thread
from flask import Flask, render_template, session, request, current_app
from flask_socketio import Namespace, emit, join_room, leave_room, \
    close_room, rooms, disconnect

import threading
import random
import time
from datetime import datetime
namespace = '/rt_data'


def get_trending(topics: list, time_start: float, time_end: float):
    r = dict()
    for t in topics:
        r[t] = generate_values(t, time_start, time_end)
    return r


ROOMS_TOPICS = dict()
TOPICS = set()
VALUES = dict()


def add_topic_room(room: str, topics: list):
    _topics = ROOMS_TOPICS.get(room, set())
    _topics.update(topics)
    TOPICS.update(topics)
    ROOMS_TOPICS[room] = _topics


def emit_room(topic, value, t=None):
    t = t if t else time.time()*1000
    data = {topic: (t, value)}
    for r, t in ROOMS_TOPICS.items():
        if topic in t:
            print("emit RTVALUES room:%s data:%r" % (r, data))
            socketio.emit("RTVALUES", data, room=r, namespace=namespace)


thread = None
thread_lock = Lock()
capp = None


def generate_values(topic: str, time_start: float, time_end: float, len: int=1000):
    if time_end <= 0 or time_start <= 0:
        return
    paso = (time_end-time_start)/len

    _vs = []
    vx, vy = (time_start, (random.random()-0.5)*100)
    for i in range(len-1):
        delta = (random.random()-0.5)*4
        vy = vy + delta
        _vs.append((vx+paso*i, vy))
    VALUES[topic] = (vx+paso*i, vy)
    return _vs


vs = generate_values("/topico1", 1, 100, 50)
print("generate_values", vs)


def background_thread():
    """Example of how to send server generated events to clients."""
    print("background_thread in new thread:")

    while True:
        socketio.sleep(1)
        print("background_thread...")
        for t in TOPICS:
            if t not in VALUES:
                VALUES[t] = (time.time()*1000, (random.random()-0.5)*100)
            vy = VALUES[t][1]
            delta = (random.random()-0.5)*4
            vy = vy + delta
            VALUES[t] = (time.time()*1000, vy)
            emit_room(t, VALUES[t][1], VALUES[t][0])

        # value = random.random()
        # emit_room("/topico1", value)


class RtData(Namespace):
    def on_connect(self):
        global thread, capp
        with thread_lock:
            if thread is None:
                thread = socketio.start_background_task(
                    target=background_thread)

    def on_disconnect(self):
        pass

    def on_get_trending(self, message):
        time_start = message.get("time_start", 0)
        time_end = message.get("time_end", None)
        topics = message.get("topics", [])
        room = message.get("room", None)

        print(topics)

        if room and message.get("mqtt_subscribe", False):
            join_room(room)
            add_topic_room(room, topics)
            topics_qos = [(topic, 0) for topic in topics]
            mqttc.subscribe(topics_qos)

        message['data'] = get_trending(topics, time_start, time_end)
        emit('trending_values', message)

    def on_mqtt_subscribe(self, message):
        topics = message.get("topics", [])
        room = message.get("room", None)
        if room:
            join_room(room)
            add_topic_room(room, topics)
            topics_qos = [(topic, 0) for topic in topics]
            mqttc.subscribe(topics_qos)

    def on_pingServer(self, message):
        message['time'] = str(datetime.now())
        print("on_pingServer", message)
        emit('pongServer', message)

    def on_testRtValue(self, message):
        print("testRtValue msg:", message)
        topics = message.get("topics", [])
        room = message.get("room", None)
        value = message.get("value", 0)
        if room:
            join_room(room)
            add_topic_room(room, topics)
            for t in topics:
                emit_room(t, value)


def on_message(client, userdata, message):
    print("mqtt on_message", message)
    emit_room(message.topic, message.payload.decode())
    # data = dict(
    #     topic=message.topic,
    #     payload=message.payload.decode(),
    #     retain=message.retain or False,
    #     qos=message.qos or 0
    # )
    # emit('mqtt_on_message', data=data)


def on_disconnect(client, userdata, self):
    print("on_disconnect client", client)
    print("userdata", userdata)
    print("self", self)


def on_connect(client, userdata, flags, rc):
    print("on_connect client", client)
    print("userdata", userdata)
    print("flags", flags)
    print("rc", rc)
    mqttc.subscribe("/#", 0)
    # mqttc.subscribe("$SYS/#", 0)


mqttc.on_message = on_message
mqttc.on_disconnect = on_disconnect
mqttc.on_connect = on_connect


socketio.on_namespace(RtData(namespace))
