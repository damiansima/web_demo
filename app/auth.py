from flask import Flask, abort, request, jsonify, g, url_for
from flask_httpauth import HTTPBasicAuth
from app.model.users import User
from app.model import session


auth = HTTPBasicAuth()


@auth.verify_password
def fn_verify_password(username_or_token, password):
    print('Authorization:', request.headers.get('authorization'))
    print("Username:", username_or_token)

    if request.headers.get('authorization'):
        # first try to authenticate by token
        authorization = request.headers.get('authorization').split(" ")
        if authorization[0] == 'Bearer' and len(authorization) > 1:
            user = User.verify_auth_token(authorization[1])
        else:
            user = User.verify_auth_token(username_or_token)
        if not user:
            # try to authenticate with username/password
            # user = session.query(User).filter_by(
            #     username=username_or_token).first()
            user = User.get_user_by_name(username_or_token)
            if not user or not user.verify_password(password):
                return False
        print("verify_password:", user)
        g.user = user
        return True
    return False


# from functools import wraps
# from app.schema import user_schema
# from pprint import pprint
# from app.utils import memorize
# from app.model import permissions
#
# @memorize.memoize
# def get_user_permissions(module: str, username: str):
#     try:
#         mod = permissions.Module.query.filter(
#             permissions.Module.name == module).first()
#         return (user_schema.ModuleSchema(many=False).dump(mod).data['users'][username])
#     except:
#         return None
#
#
# def permission_module(module, create=False, read=False, update=False, delete=False):
#     def wrapper(f):
#         @wraps(f)
#         def wrapped(*args, **kwargs):
#             fail = False
#             if g.user is None:
#                 fail = True
#             else:
#                 perm = get_user_permissions(module, g.user.username)
#                 print("perm:", perm)
#
#                 if create and not perm.c:
#                     fail = True
#                 if read and not perm.r:
#                     fail = True
#                 if update and not perm.u:
#                     fail = True
#                 if delete and not perm.d:
#                     fail = True
#
#             if not fail:
#                 return f(*args, **kwargs)
#             abort(403)
#
#         return wrapped
#     return wrapper

from functools import wraps
from app.model import modules_permissions


def permission_module(module_name, create=False, read=False, update=False, delete=False):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if g.user is not None:
                print("permission_module", module_name,
                      g.user, create, read, update, delete)
                if modules_permissions.fn_permission_module(module_name, g.user, create, read, update, delete):
                    return f(*args, **kwargs)
            abort(403)
        return wrapped
    return wrapper
