
from flask import request, g, jsonify, abort
from flask_restplus import Api, fields
from flask_restplus import marshal 
from app.api import api_rest
from app.auth import auth,permission_module
from app.api.rest.base import BaseResource, SecureResource
from app.model import  session, users, AddDb, UPdateDb, DeleteDb
from app.schema import user_schema, paginate, pagination_arguments ,get_filter_paginate 

ns_user = api_rest.namespace('users', description='Users Operations') 
 
@ns_user.route('/token')
class ResourceToken(SecureResource): 
 
    def get(self):
        token = g.user.generate_auth_token()
        return jsonify({
            'username': g.user.username,
            'token': token.decode('ascii'),
            'duration': 600 ,
            'user_id':g.user.id
        })
 
user_register_model = api_rest.model(
    'UserRegisterModel', {
        'username':
        fields.String(description="User to be registered", required=True),
        'password':
        fields.String(description="Password", required=True)
    })


@ns_user.route('/auth_token')
class ResourceAuthToken(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(user_register_model, validate=True)
    def post(self):
        data = marshal(request.json, user_register_model)
        username = data['username']
        password = data['password']

        user = session.query(users.User).filter_by(username=username).first()
        if user and user.verify_password(password):
            token = user.generate_auth_token()
            return jsonify({
                'user_id':user.id,
                'username': user.username,
                'token': token.decode('ascii'),
                'duration': 600
            })
        abort(400)


 
user_model = api_rest.model(
    'UserModel', {
        'username':
        fields.String(description="User to be registered", required=True),
        'password':
        fields.String(description="Password", required=True),
        'name':
        fields.String(description="Real Name", required=True),
        'email':
        fields.String(description="Real Name", required=False),
    })

user_model_update = api_rest.model(
    'UserModelUpdate', {
        'name':
        fields.String(description="User to be registered", required=True),
        'password':
        fields.String(description="Password", required=True),
        'email':
        fields.String(description="Email", required=True),
    })    

@ns_user.route('/<int:id>')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class UserDetailResource(BaseResource):
    def get(self, id):
        user = session.query(users.User).get(id)
        if user is None:
            abort(404)
        return user_schema.UserSchema().dump(user).data

    def delete(self, id):
        user = session.query(users.User).get(id)
        if user is None:
            abort(404)
        if not DeleteDb(user):
            abort(404)
        return {'id': user.id}

    @ns_user.expect(user_model_update, validate=True)
    def put(self, id):
        data = marshal(request.json, user_model_update)
        user = session.query(users.User).get(id)
        if user is None:
            abort(400)

        name = data["name"]
        password = data["password"]
        email = data["email"]

        if email: 
            user.email=email
        if name: 
            user.name=name
        if password: 
            user.hash_password(password)

        if not UPdateDb(user):
            abort(404)

        return {'username': user.username}


@ns_user.route('')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class UserResource(BaseResource):
    """ Sample Resource Class """
 
    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return jsonify(
            get_filter_paginate(users.User, user_schema.UserSchema(many=True,only=['id', 'name','username']) )) 

    @ns_user.expect(user_model, validate=True)
    def post(self):
        data = marshal(request.json, user_model)
        user = users.User.add_user(**data)
        if user is None:
            abort(400)
        return {'username': user.username}



group_model = api_rest.model(
    'GroupModel', {
        'name': fields.String(description="Group", required=True),
        'description': fields.String(
            description="Description", required=False),
        'id': fields.Integer(description="Id", required=False),
    })


@ns_user.route('/groups')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class GroupResource(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return paginate(
            user_schema.GroupSchema(many=True, only=['id', 'name']),
            session.query(users.Group))

    @ns_user.expect(group_model, validate=True)
    def post(self):
        data = marshal(request.json, group_model)
        perm = users.Group(**data)
        if not AddDb(perm):
            abort(404)
        return {'group_model': data}


group_user_model = api_rest.model(
    'GroupUserModel', {
        'name': fields.String(description="Group", required=False),
        'description': fields.String(
            description="Description", required=False),
        'user_id': fields.Integer(description="User ID", required=False)
    })


@ns_user.route('/groups/<int:id>')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class GroupDetailResource(SecureResource):
    """ Sample Resource Class """

    @permission_module("Usuarios", read=True)
    def get(self, id):
        group = session.query(users.Group).get(id)
        if group is None:
            abort(404)
        return user_schema.GroupSchema().dump(group).data

    @permission_module("Usuarios", delete=True)
    def delete(self, id):
        group = session.query(users.Group).get(id)
        if group is None:
            abort(404)
        if not DeleteDb(group):
            abort(404)
        return {'id': group.id}

    @permission_module("Usuarios", create=True)
    @ns_user.expect(group_user_model, validate=True)
    def put(self, id):
        data = marshal(request.json, group_user_model)
        name = data['name']
        description = data['description']
        user_id = data['user_id']
        group = session.query(users.Group).get(id)
        if group is None:
            abort(404)
        if session.query(users.Group).filter(
                users.Group.name == name).first() is not None:
            abort(409)
        if name:
            group.name = name
        if description:
            group.description = description
        if name or description:
            if not UPdateDb(group):
                abort(404)
        if user_id:
            user = session.query(users.User).get(user_id)
            print("add user:", user)
            if user:
                ug = users.UserGroup(user=user, group=group)
                AddDb(ug)

        return {'data': data}
