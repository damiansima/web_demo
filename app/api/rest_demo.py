
from flask import request, g, jsonify, abort
from flask_restplus import Api, fields
from flask_restplus import marshal
from app.api import api_rest
from app.auth import auth
from app.api.rest.base import BaseResource, SecureResource
from app.model import users, AddDb, UPdateDb, DeleteDb
from app.schema import user_schema, paginate, pagination_arguments, get_filter_paginate
from datetime import datetime


@api_rest.route('/resource/<string:resource_id>')
class ResourceOne(BaseResource):
    """ Sample Resource Class """

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}

    def post(self, resource_id):
        json_payload = request.json
        return {'timestamp': json_payload}, 201


@api_rest.route('/echo_resource_id/<string:resource_id>')
class EchoResourceID(BaseResource):
    """ Sample Resource Class """

    # method_decorators = [auth.login_required]

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {resource_id: timestamp}


@api_rest.route('/secure-resource/<string:resource_id>')
class SecureResourceOne(SecureResource):
    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}
