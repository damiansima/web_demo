"""
REST API Resource Routing
http://flask-restplus.readthedocs.io
"""

from datetime import datetime
from flask import request, g, jsonify, abort
from flask_restplus import Api, fields
from flask_restplus import marshal
from app.api.rest.base import BaseResource, SecureResource
from app.api import api_rest
from app.auth import auth
from app.model import db, permissions, users, AddDb, UPdateDb, DeleteDb
from app.schema import user_schema, paginate, get_filter_paginate, pagination_arguments
from app.model import generic_content

ns_user = api_rest.namespace('users', description='Users Operations')
ns_content = api_rest.namespace('content', description='Content Operations')


@ns_user.route('/token')
class ResourceToken(BaseResource):
    """ Sample Resource Class """

    method_decorators = [auth.login_required] 
    def get(self):
        token = g.user.generate_auth_token()
        return jsonify({
            'username': g.user.username,
            'token': token.decode('ascii'),
            'duration': 600 ,
            'user_id':g.user.id
        })


user_register_model = api_rest.model(
    'UserRegisterModel', {
        'username':
        fields.String(description="User to be registered", required=True),
        'password':
        fields.String(description="Password", required=True)
    })


@ns_user.route('/auth_token')
class ResourceAuthToken(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(user_register_model, validate=True)
    def post(self):
        data = marshal(request.json, user_register_model)
        username = data['username']
        password = data['password']

        user = users.User.query.filter_by(username=username).first()
        if user and user.verify_password(password):
            token = user.generate_auth_token()
            return jsonify({
                'user_id':user.id,
                'username': user.username,
                'token': token.decode('ascii'),
                'duration': 600
            })
        abort(400)


user_model = api_rest.model(
    'UserModel', {
        'username':
        fields.String(description="User to be registered", required=True),
        'password':
        fields.String(description="Password", required=True),
        'name':
        fields.String(description="Real Name", required=True),
    })


@ns_user.route('')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class ResourceUser(BaseResource):
    """ Sample Resource Class """

    def get(self, id):
        user = users.User.query.get(id)
        if user is None:
            abort(404)
        return user_schema.UserSchema().dump(user).data

    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return jsonify(
            paginate(user_schema.UserSchema(many=True), users.User.query))
        # return paginate(user_schema.UserSchema(many=True), User.query.filter(User.username == 'sima'))

    @ns_user.expect(user_model, validate=True)
    def post(self):
        data = marshal(request.json, user_model)
        user = users.User.add_user(**data)
        if user is None:
            abort(400)
        return {'username': user.username}


group_model = api_rest.model(
    'GroupModel', {
        'name': fields.String(description="Group", required=True),
        'description': fields.String(
            description="Description", required=False),
        'id': fields.Integer(description="Id", required=False),
    })


@ns_user.route('/groups')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class GroupResource(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return paginate(
            user_schema.GroupSchema(many=True, only=['id', 'name']),
            users.Group.query)

    @ns_user.expect(group_model, validate=True)
    def post(self):
        data = marshal(request.json, group_model)
        perm = users.Group(**data)
        if not AddDb(perm):
            abort(404)
        return {'group_model': data}


group_user_model = api_rest.model(
    'GroupUserModel', {
        'name': fields.String(description="Group", required=False),
        'description': fields.String(
            description="Description", required=False),
        'user_id': fields.Integer(description="User ID", required=False)
    })


@ns_user.route('/groups/<int:id>')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class GroupDetailResource(BaseResource):
    """ Sample Resource Class """

    def get(self, id):
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        return user_schema.GroupSchema().dump(group).data

    def delete(self, id):
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        if not DeleteDb(group):
            abort(404)
        return {'id': group.id}

    # @auth.login_required
    @ns_user.expect(group_user_model, validate=True)
    def put(self, id):
        data = marshal(request.json, group_user_model)
        name = data['name']
        description = data['description']
        user_id = data['user_id']
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        if users.Group.query.filter(
                users.Group.name == name).first() is not None:
            abort(409)
        if name:
            group.name = name
        if description:
            group.description = description
        if name or description:
            if not UPdateDb(group):
                abort(404)
        if user_id:
            user = users.User.query.get(user_id)
            print("add user:", user)
            if user:
                ug = users.UserGroup(user=user, group=group)
                AddDb(ug)

        return {'group_user_model': data}


module_model = api_rest.model(
    'ModuleModel', {
        'name': fields.String(description="Group", required=True),
        'description': fields.String(
            description="Description", required=False),
        'id': fields.Integer(description="Id", required=False),
    })


@ns_user.route('/modules')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class ModulesResource(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return paginate(
            user_schema.ModuleSchema(many=True, only=['id', 'name']),
            permissions.Module.query)

    @ns_user.expect(module_model, validate=True)
    def post(self):
        data = marshal(request.json, module_model)
        perm = permissions.Module(**data)
        if not AddDb(perm):
            abort(404)
        return {'module_model': data}


# module_detail_model = api_rest.model('ModuleDetailModel', {
#     'name': fields.String(description="Group", required=False),
#     'description': fields.String(description="Description", required=False),
#     'owner_id': fields.Integer(description="User ID", required=False),
#     'group_id': fields.Integer(description="Group ID", required=False)
# })


@ns_user.route('/modules/<int:id>')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class ModuleDetailResource(BaseResource):
    """ Sample Resource Class """

    def get(self, id):
        mod = permissions.Module.query.get(id)
        if mod is None:
            abort(404)
        return user_schema.ModuleSchema().dump(mod).data

    def delete(self, id):
        mod = permissions.Module.query.get(id)
        if mod is None:
            abort(404)
        if not DeleteDb(mod):
            abort(404)
        return {'id': mod.id}


module_permission_model = api_rest.model(
    'ModuleModel', {
        'module_id': fields.Integer(description="Id", required=False),
        'owner_id': fields.Integer(description="Id", required=False),
        'group_id': fields.Integer(description="Id", required=False),
        'c': fields.Boolean(
            description="Create", default=False, required=False),
        'r': fields.Boolean(description="Read", default=False, required=False),
        'u': fields.Boolean(
            description="Update", default=False, required=False),
        'd': fields.Boolean(
            description="Delete", default=False, required=False),
    })


@ns_user.route('/modules/permissions')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class ModulesPermissionsResource(BaseResource):
    """ Sample Resource Class """

    @ns_user.expect(pagination_arguments)
    def get(self):
        print("user get")
        return paginate(
            user_schema.ModulePermissionSchema(many=True),
            permissions.ModulePermission.query)

    @ns_user.expect(module_permission_model, validate=True)
    def post(self):
        data = marshal(request.json, module_permission_model)
        # perm = permissions.ModulePermission(**data)
        # if not AddDb(perm):
        #     abort(404)
        return {'module_model': data}


@ns_user.route('/modules/permissions/<int:id>')
@ns_user.response(200, 'Success')
@ns_user.response(400, 'Validation Error')
class ModulesPermissionsDetailResource(BaseResource):
    """ Sample Resource Class """

    def get(self, id):
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        return user_schema.GroupSchema().dump(group).data

    def delete(self, id):
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        if not DeleteDb(group):
            abort(404)
        return {'id': group.id}

    # @auth.login_required
    @ns_user.expect(group_user_model, validate=True)
    def put(self, id):
        data = marshal(request.json, group_user_model)
        name = data['name']
        description = data['description']
        user_id = data['user_id']
        group = users.Group.query.get(id)
        if group is None:
            abort(404)
        if users.Group.query.filter(
                users.Group.name == name).first() is not None:
            abort(409)
        if name:
            group.name = name
        if description:
            group.description = description
        if name or description:
            if not UPdateDb(group):
                abort(404)
        if user_id:
            user = users.User.query.get(user_id)
            print("add user:", user)
            if user:
                ug = users.UserGroup(user=user, group=group)
                AddDb(ug)

        return {'group_user_model': data}


generic_content_model = api_rest.model(
    'GenericContentModel', {
        'group_id':
        fields.Integer(description="Id", required=False),
        'content_name':
        fields.String(description="Nombre de Contenido", required=False),
        'content':
        fields.Raw(
            description="Nombre de Contenido",
            default={
                'objeto1': 'nada',
                'objeto2': 21
            },
            required=False),
        'is_visible':
        fields.Boolean(description="Visible", default=True, required=False),
        'is_editable':
        fields.Boolean(description="Editable", default=True, required=False),
        'is_enabled':
        fields.Boolean(description="Enable", default=True, required=False),
    })


@ns_content.route('')
@ns_content.response(200, 'Success')
@ns_content.response(400, 'Validation Error')
class GenericContentResouce(BaseResource):
    """ Sample Resource Class """

    @ns_content.expect(pagination_arguments)
    def get(self):
        print("user get")
        return get_filter_paginate(
            generic_content.GenericContent,
            user_schema.GenericContentSchema(
                many=True,
                only=['id', 'content_name', "content", "owner_id",
                      "group_id"]))
        # return paginate(user_schema.GenericContentSchema(many=True, only=['id', 'content_name',"content","owner_id","group_id"]), generic_content.GenericContent.query)

    @auth.login_required
    @ns_content.expect(generic_content_model, validate=False)
    def post(self):
        data = marshal(request.json, generic_content_model)
        gc = generic_content.GenericContent.add_content(
            g.user.username, data['content_name'], data['content'])
        # perm = permissions.ModulePermission(**data)
        # if not AddDb(perm):
        #     abort(404)
        return {'generic_content': gc.content if gc else None}


""" 

@app.route('/api/users', methods=['POST'])
def new_user():
    print('ACACACACCACC',request.form)
    username = request.json.get('username')
    password = request.json.get('password')
    print(username, password)
    if username is None or password is None:
        abort(400)  # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        abort(400)  # existing user
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()

    print(user.id)
    return (jsonify({'username': user.username}), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/api/users/<int:id>')
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'username': user.username})

"""


@api_rest.route('/test_login_required2')
class ResourceOne2(BaseResource):
    """ Sample Resource Class """

    def get(self):
        return jsonify({'data': 'Anonymous!'})


@api_rest.route('/resource/<string:resource_id>')
class ResourceOne(BaseResource):
    """ Sample Resource Class """

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}

    def post(self, resource_id):
        json_payload = request.json
        return {'timestamp': json_payload}, 201


@api_rest.route('/echo_resource_id/<string:resource_id>')
class EchoResourceID(BaseResource):
    """ Sample Resource Class """

    # method_decorators = [auth.login_required]

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {resource_id: timestamp}


@api_rest.route('/secure-resource/<string:resource_id>')
class SecureResourceOne(SecureResource):
    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}
