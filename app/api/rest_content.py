from flask import request, g, jsonify, abort
from flask_restplus import Api, fields
from flask_restplus import marshal
from app.api import api_rest
from app.auth import auth
from app.api.rest.base import BaseResource, SecureResource
from app.model import users, AddDb, UPdateDb, DeleteDb
from app.schema import user_schema, get_filter_paginate, pagination_arguments
from app.model import generic_content

ns_content = api_rest.namespace('content', description='Content Operations')

generic_content_model = api_rest.model(
    'GenericContentModel', {

        'content_name':
        fields.String(description="Nombre de Contenido", required=False),
        'content':
        fields.Raw(
            description="Nombre de Contenido",
            default={
                'objeto1': 'nada',
                'objeto2': 21
            },
            required=False)
    })


@ns_content.route('')
@ns_content.response(200, 'Success')
@ns_content.response(400, 'Validation Error')
class GenericContentResouce(BaseResource):
    """ Sample Resource Class """

    @ns_content.expect(pagination_arguments)
    def get(self):
        print("user get")
        return get_filter_paginate(generic_content.GenericContent,
                                   generic_content.GenericContentSchema(only=['id', 'content_name'], many=True))

    @auth.login_required
    @ns_content.expect(generic_content_model, validate=False)
    def post(self):
        data = marshal(request.json, generic_content_model)
        gc = generic_content.GenericContent.add_content(
            g.user.username, data['content_name'], data['content'])

        return {'generic_content': gc.content if gc else None}


@ns_content.route('/permission')
@ns_content.response(200, 'Success')
@ns_content.response(400, 'Validation Error')
class GenericContentPermissionResouce(BaseResource):
    """ Sample Resource Class """

    @ns_content.expect(pagination_arguments)
    def get(self):
        return get_filter_paginate(generic_content.ContentPermission, generic_content.ContentPermissionSchema(many=True))


@ns_content.route('/<int:id>')
@ns_content.response(200, 'Success')
@ns_content.response(400, 'Validation Error')
class UserDetailResource(BaseResource):

    def get(self, id):
        cont = generic_content.GenericContent.query.get(id)
        if cont is None:
            abort(404)
        return generic_content.GenericContentSchema(only=['id', 'content_name', "content", "owners", "groups"]).dump(cont).data
