from functools import wraps
import time


def get_current_user_role():
    return ('admin', 'editor')


def error_response():
    print("No se pudo validar")
    return False


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            user_roles = get_current_user_role()
            for r in roles:
                if r in user_roles:
                    return f(*args, **kwargs)
            return error_response()

        return wrapped
    return wrapper


def memorize(timeout=120):
    def wrapper(func):
        cache = func._cache = {}

        @wraps(func)
        def memoized_func(*args, **kwargs):
            key = str(args) + str(kwargs)
            if key not in cache or cache[key][0] < time.time()-timeout:
                cache[key] = (time.time(), func(*args, **kwargs))
                print("- memorizing: [%r]:%r" % (key, cache[key]))
            else:
                print("- memorized: [%r]:%r" % (key, cache[key]))
            return cache[key][1]

        return memoized_func
    return wrapper


def clear_memoize(func):
    func._cache = {}


if __name__ == "__main__":

    @memorize
    def fib(n):
        if n == 0:
            return 0
        if n == 1:
            return 1
        else:
            return fib(n-1) + fib(n-2)

    def fibonacci(n):
        if n == 0:
            return 0
        if n == 1:
            return 1
        else:
            return fib(n-1) + fib(n-2)

    print(fibonacci(40))
    print(fibonacci(40))
    print(fibonacci(40))
    print(fib._cache)
    clear_memoize(fib)
    print(fib._cache)

    @requires_roles('visual', 'editor', 'nada')
    def test():
        print("----- Test OK")

    @requires_roles('visual', 'asdf', 'asdf')
    def test2():
        print("----- Test OK")
    test()
    test2()
