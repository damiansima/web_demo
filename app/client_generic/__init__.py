""" Client App """

from flask import Blueprint, render_template

client_gen_bp = Blueprint('client_generic', __name__,
                          url_prefix='/client_generic',
                          static_url_path='',
                          static_folder='./static',
                          template_folder='./templates',
                          )


@client_gen_bp.route('/')
@client_gen_bp.route('/<name>')
def hello(name=None):
    return render_template('hello.html', name=name, dato=[])


@client_gen_bp.route('/index_soketio')
def index_soketio(name=None):
    return render_template('index_soket.io.html')


from app.client_generic import rt_socketio
from app.socketio import socketio
socketio.on_namespace(rt_socketio.MyNamespace('/rt'))
