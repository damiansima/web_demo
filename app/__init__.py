#!/usr/bin/env python
# -*- coding: utf-8 -*-
# activate eventlet
# import eventlet
import os
from flask import Flask, redirect, url_for, send_from_directory
import datetime
from time import sleep


# eventlet.monkey_patch()
app = Flask(__name__)

from . import config
app.config.from_object('app.config.{}'.format(config.flask_config))
app.logger.info('>>> {}'.format(config.flask_config))

# Cliente generico
from app.client_generic import client_gen_bp
app.register_blueprint(client_gen_bp)

# Cliente hecho con Vue.Js
from app.client_vue import client_vue_bp
app.register_blueprint(client_vue_bp)

from app.model.users import create_example_users
from app.model import generic_content
from app.model import modules_permissions

# from app.model import db
# db.init_app(app)
with app.test_request_context():
    # db.create_all()

    from app.model import Base, engine
    Base.metadata.create_all(engine)
    create_example_users()

from app.api import api_rest, api_bp
app.register_blueprint(api_bp)

from app.socketio import socketio, mqttc, rt_data
socketio.init_app(app)
mqttc.loop_start()


@app.after_request
def add_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@app.route('/')
def hello():
    return redirect('/client_vue')


# a short running task that returns immediately
@app.route('/shortTask')
def short_running_task():
    start = datetime.datetime.now()
    return 'Started at {0}, returned at {1}'.format(start, datetime.datetime.now())


@app.route('/longTask')
def long_running_task():
    start = datetime.datetime.now()
    sleep(30)
    return 'Started at {0}, returned at {1}'.format(start, datetime.datetime.now())


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')


# from app.client_generic import client_gen_bp
# from app.socketio import socketio
#
# app.register_blueprint(api_bp)
# app.register_blueprint(client_gen_bp)
# socketio.init_app(app)
#
# from app.model import db
# from app.model.permissions import create_example
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
# app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = False
#
# db.init_app(app)
# with app.test_request_context():
#     db.create_all()
#     create_example()
#
#     import app.schema as sch
#     from app.model import permissions, users
#
#     print("******** ACA *********")
#     f, o = sch.to_filter(
#         users.User,
#         # [{"name": "username", "op": "like", "val": "sima"}],
#         [{"name": "id", "op": ">", "value": "5"}],
#         [{"field": "id", "direction": "asc"}])
#     for i in f:
#         print("--", i)
#
#     gg = users.User.query.filter(*f).order_by(*o)
#     print(gg)
#     print(gg.all())
#     print("end")
#
#
