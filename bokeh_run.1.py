from bokeh.io import curdoc
from bokeh.layouts import row
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, HoverTool, CustomJS

x = list(range(-20, 21))
y0 = [abs(xx) for xx in x]
y1 = [xx**2 for xx in x]

# can be either the same source for all plots or different sources (as shown here) but the length of the sources should be the same
source1 = ColumnDataSource(data=dict(x=x, y0=y0, y1=y1))
source2 = ColumnDataSource(data=dict(x=x, y0=y0, y1=y1))
code1 = "source1.set('selected', cb_data['index']);"
code2 = "source2.set('selected', cb_data['index']);"
callback = CustomJS(
    args={'source1': source1, 'source2': source2}, code=code1+code2)

p1 = figure(tools=[], width=300, height=300, title=None, toolbar_location=None)
p2 = figure(tools=[], width=300, height=300, title=None, toolbar_location=None)
c1 = p1.circle(x='x', y='y0', source=source1)
c2 = p2.circle(x='x', y='y1', source=source2)
hover1 = HoverTool(callback=callback, renderers=[c1])
hover2 = HoverTool(callback=callback, renderers=[c2])
p1.add_tools(hover1)
p2.add_tools(hover2)

doc_layout = row([p1, p2])
curdoc().add_root(doc_layout)
